#      A python library for getting Load Shedding schedules.
#      Copyright (C) 2021  Werner Pieterson
#
#      This program is free software: you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation, either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.

from unittest import TestCase

from load_shedding import get_stage, get_schedule, get_providers, list_to_dict, StageError
from load_shedding.providers.eskom import Province, Stage, Suburb
from tests.mock_eskom import MockEskom


class Test(TestCase):
    def setUp(self) -> None:
        self.eskom = MockEskom()

    def test_get_stage(self):
        got = get_stage(self.eskom)
        self.assertEqual(Stage.NO_LOAD_SHEDDING, got)

    def test_get_schedule(self):
        self.assertRaises(StageError, get_schedule, self.eskom, province=Province.WESTERN_CAPE,
                          suburb=Suburb(id=1058852), stage=Stage.UNKNOWN, cached=True)

        want = [
            ["2022-06-26T12:00:00+00:00", "2022-06-26T14:30:00+00:00"], ["2022-06-26T20:00:00+00:00", "2022-06-26T22:30:00+00:00"],
            ["2022-06-27T20:00:00+00:00", "2022-06-27T22:30:00+00:00"], ["2022-06-28T04:00:00+00:00", "2022-06-28T06:30:00+00:00"],
            ["2022-06-29T02:00:00+00:00", "2022-06-29T04:30:00+00:00"], ["2022-06-29T10:00:00+00:00", "2022-06-29T12:30:00+00:00"],
            ["2022-06-30T10:00:00+00:00", "2022-06-30T12:30:00+00:00"], ["2022-06-30T18:00:00+00:00", "2022-06-30T20:30:00+00:00"],
            ["2022-07-01T16:00:00+00:00", "2022-07-01T18:30:00+00:00"], ["2022-07-02T00:00:00+00:00", "2022-07-02T02:30:00+00:00"],
            ["2022-07-03T00:00:00+00:00", "2022-07-03T02:30:00+00:00"], ["2022-07-03T08:00:00+00:00", "2022-07-03T10:30:00+00:00"],
            ["2022-07-04T08:00:00+00:00", "2022-07-04T10:30:00+00:00"], ["2022-07-04T16:00:00+00:00", "2022-07-04T18:30:00+00:00"],
            ["2022-07-05T14:00:00+00:00", "2022-07-05T16:30:00+00:00"], ["2022-07-05T22:00:00+00:00", "2022-07-06T00:30:00+00:00"],
            ["2022-07-06T22:00:00+00:00", "2022-07-07T00:30:00+00:00"], ["2022-07-07T06:00:00+00:00", "2022-07-07T08:30:00+00:00"],
            ["2022-07-08T06:00:00+00:00", "2022-07-08T08:30:00+00:00"], ["2022-07-08T14:00:00+00:00", "2022-07-08T16:30:00+00:00"],
            ["2022-07-09T12:00:00+00:00", "2022-07-09T14:30:00+00:00"], ["2022-07-09T20:00:00+00:00", "2022-07-09T22:30:00+00:00"],
            ["2022-07-10T20:00:00+00:00", "2022-07-10T22:30:00+00:00"], ["2022-07-11T04:00:00+00:00", "2022-07-11T06:30:00+00:00"],
            ["2022-07-12T04:00:00+00:00", "2022-07-12T06:30:00+00:00"], ["2022-07-12T12:00:00+00:00", "2022-07-12T14:30:00+00:00"],
            ["2022-07-13T10:00:00+00:00", "2022-07-13T12:30:00+00:00"], ["2022-07-13T18:00:00+00:00", "2022-07-13T20:30:00+00:00"],
            ["2022-07-14T18:00:00+00:00", "2022-07-14T20:30:00+00:00"], ["2022-07-15T02:00:00+00:00", "2022-07-15T04:30:00+00:00"],
            ["2022-07-16T02:00:00+00:00", "2022-07-16T04:30:00+00:00"], ["2022-07-16T10:00:00+00:00", "2022-07-16T12:30:00+00:00"],
            ["2022-07-17T08:00:00+00:00", "2022-07-17T10:30:00+00:00"], ["2022-07-17T16:00:00+00:00", "2022-07-17T18:30:00+00:00"],
            ["2022-07-18T16:00:00+00:00", "2022-07-18T18:30:00+00:00"], ["2022-07-19T00:00:00+00:00", "2022-07-19T02:30:00+00:00"],
            ["2022-07-20T00:00:00+00:00", "2022-07-20T02:30:00+00:00"], ["2022-07-20T08:00:00+00:00", "2022-07-20T10:30:00+00:00"],
            ["2022-07-21T06:00:00+00:00", "2022-07-21T08:30:00+00:00"], ["2022-07-21T14:00:00+00:00", "2022-07-21T16:30:00+00:00"],
            ["2022-07-22T14:00:00+00:00", "2022-07-22T16:30:00+00:00"], ["2022-07-22T22:00:00+00:00", "2022-07-23T00:30:00+00:00"],
            ["2022-07-23T22:00:00+00:00", "2022-07-24T00:30:00+00:00"], ["2022-07-24T06:00:00+00:00", "2022-07-24T08:30:00+00:00"],
        ]
        got = get_schedule(self.eskom, province=Province.WESTERN_CAPE, suburb=Suburb(id=1058852),
                           stage=Stage.STAGE_2, cached=True)
        self.assertEqual(list, type(got))
        self.assertEqual(list, type(got[0]))
        self.assertListEqual(want, got)

    def test_list_to_dict(self):
        schedule = get_schedule(self.eskom, province=Province.WESTERN_CAPE, suburb=Suburb(id=1058852),
                           stage=Stage.STAGE_2, cached=True)
        self.assertEqual(list, type(schedule))
        got = list_to_dict(schedule)
        self.assertEqual(dict, type(got))

    def test_get_providers(self):
        got = get_providers()
        self.assertEqual(list, type(got))

        want = ["Eskom"]
        self.assertEqual(want, [provider.name for provider in got])


